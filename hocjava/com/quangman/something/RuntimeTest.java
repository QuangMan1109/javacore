package hocjava.com.quangman.something;

import java.io.IOException;

public class RuntimeTest {
    public static void main(String[] args) throws IOException {

        Runtime runtime = Runtime.getRuntime();

        // //Open notepad
        // runtime.exec("notepad");

        //processor
        System.out.println(Runtime.getRuntime().availableProcessors());

        //memory
        System.out.println("Total memory = " + runtime.totalMemory());
        System.out.println("Free memory = " + runtime.freeMemory());
        for (int i = 0; i < 1000; i++) {
            new RuntimeTest();
        }
        System.out.println("Before calling gc method: " + runtime.freeMemory());
        System.gc();
        System.out.println("After calling gc method: " + runtime.freeMemory());

    }
}
