package hocjava.com.quangman.something;

public class ThreadTest extends Thread {

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println(Thread.currentThread().getName() + " " + i);
        }
    }

    public static void main(String[] args) {
        Thread thread1 = new ThreadTest();
        Thread thread2 = new ThreadTest();
        Thread thread3 = new ThreadTest();

        thread1.setName("A");
        thread2.setName("B");
        thread3.setName("C");

        System.out.println("thread1 id = " + thread1.getName());
        System.out.println("thread2 id = " + thread2.getName());
        System.out.println("thread3 id = " + thread3.getName());

        thread1.start();
        try {
            thread1.join(1500);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        thread2.start();
        thread3.start();

        System.out.println("hello world");
    }
}
