package hocjava.com.quangman.something;

public class SyncMethodTest {
    public static void main(String[] args) {
        int n = 1;
        Dog dog = new Dog(n);
        MyThread1 myThread1 = new MyThread1(dog);
        MyThread2 myThread2 = new MyThread2(dog);

        myThread1.start();
        myThread2.start();

    }
}

class Dog {
    static int n;

    public Dog(int n) {
        Dog.n = n;
    }

    synchronized public int print() {
        for (int i = 1; i <= 5; i++) {
            n *= i;
            System.out.println("n = " + n);
        }
        return n;
    }
    
}

class MyThread1 extends Thread {
    public Dog dog;

    public MyThread1(Dog dog) {
        this.dog = dog;
    }
    
    @Override
    public void run() {
        System.out.println("A " + dog.print());
    }
}

class MyThread2 extends Thread {
    public Dog dog;

    public MyThread2(Dog dog) {
        this.dog = dog;
    }
    
    @Override
    public void run() {
        System.out.println("B " + dog.print());
    }
}
