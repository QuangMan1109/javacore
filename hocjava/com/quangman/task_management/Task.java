package hocjava.com.quangman.task_management;

import java.util.Calendar;

public class Task {
    enum TaskType {
        Code,
        Test,
        Design,
        Review
    }

    private int id;
    private String name;
    private int type;
    private Calendar date;
    private double beginTime;
    private double endTime;
    private String assignee;
    private String expert;

    public Task() {
    }
    
    public Task(int id, String name, int type, Calendar date, double beginTime, double endTime, String assignee, String expert) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.date = date;
        this.beginTime = beginTime;
        this.endTime = endTime;
        this.assignee = assignee;
        this.expert = expert;
    }


    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Calendar getDate() {
        return this.date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public double getBeginTime() {
        return this.beginTime;
    }

    public void setBeginTime(double beginTime) {
        this.beginTime = beginTime;
    }

    public double getEndTime() {
        return this.endTime;
    }

    public void setEndTime(double endTime) {
        this.endTime = endTime;
    }

    public String getAssignee() {
        return this.assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getExpert() {
        return this.expert;
    }

    public void setExpert(String expert) {
        this.expert = expert;
    }

}
