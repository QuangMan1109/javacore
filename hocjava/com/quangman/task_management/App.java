//J1.S.P0071
//Author: Quang Man

package hocjava.com.quangman.task_management;

import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import hocjava.com.quangman.calculator_bmi.Common;

public class App {

    public static void displayMenu() {
        System.out.println("");
        System.out.println("========= Task program =========");
        System.out.println("1.	Add Task");
        System.out.println("2.	Delete task");
        System.out.println("3.	Display Task");
        System.out.println("4.	exit");
        System.out.print("Your option: ");
    }

    public static String checkOption() {
        String option;
        
        //check the validation of option input
        while (true) {
            option = Common.scanner.next();

            if (!(option.equals("1") || option.equals("2") || option.equals("3") || option.equals("4"))) {
                System.out.print("You have to input above options (1-4): ");
                continue;
            }
            else {
                break;
            }
        }

        return option;
    }

    public static Task addTask(LinkedList<Task> tasks) {
        //title
        System.out.println("------------Add Task---------------");
        
        //name
        System.out.print("Requirement Name: ");
        String nameScanner = Common.scanner.next();

        //task type: only accept integer number from 1 to 4
        int type = Common.checkIntegerInput("Task Type (1-4): ", 1, 4);

        //date
        Calendar calendar = Calendar.getInstance();
        while (true) {
            System.out.print("Date (dd-MM-YYYY): ");
            String dateScanner = Common.scanner.next();
            try {
                // date = new SimpleDateFormat("dd-MM-yyyy").parse(dateScanner);
                calendar.setTime(Common.myCalendarFormat.parse(dateScanner));
                break;
            } catch (Exception e) {
                System.out.println(e);
                continue;
            }
        }

        //beginning time
        double beginTime = Common.checkDoubleInput("From (from 8.0-17.5): ", 8.0, 17.5);

        //end time
        double endTime = Common.checkDoubleInput("To (from " + beginTime + "-17.5): ", beginTime, 17.5);

        //assignee
        System.out.print("Assignee: ");
        String assigneeScanner = Common.scanner.next();

        System.out.print("Reviewer: ");
        String reviewerScanner = Common.scanner.next();

        Task task = new Task(tasks.size(), nameScanner, type, calendar, beginTime, endTime, assigneeScanner, reviewerScanner);
        tasks.add(task);
        return task;
    }

    public static void deleteTask(LinkedList<Task> tasks) {

        int lastId = tasks.size() - 1;
        int idDeleted = 0;
        while (true) {
            System.out.print("Input the ID of the task that you want to delete, or input -1 to cancel (lastID = " + lastId + ")");
            idDeleted = Common.scanner.nextInt();

            if (idDeleted == -1) return;
    
            if (idDeleted > lastId) {
                System.out.println("There's no such that ID, please input again");
                continue;
            }

            tasks.remove(tasks.get(idDeleted));
            return;
        }
        
    }

    public static void displayTask(LinkedList<Task> tasks) {
        System.out.println("----------------------------------------- Task ---------------------------------------");
        String outRowFormat = "%3s %10s %10s %15s %5s %10s %10s\n";

        Map<Integer, String> taskType = new HashMap<Integer, String>();
        taskType.put(1, "Code");
        taskType.put(2, "Test");
        taskType.put(3, "Design");
        taskType.put(4, "Review");

        System.out.printf(outRowFormat, "ID", "Name", "Type", "Date", "Time", "Assignee", "Reviewer");
        for (Task task : tasks) {    
            String outId = task.getId() + "";
            String outName = task.getName();
            String outType = taskType.get(task.getType());
            String outDate = Common.myCalendarFormat.format(task.getDate().getTime());
            String outTime = (task.getEndTime() - task.getBeginTime()) + "";
            String outAssignee = task.getAssignee();
            String outReviewer = task.getExpert();
            
            System.out.printf(outRowFormat, outId, outName, outType, outDate, outTime, outAssignee, outReviewer);
        
        }
    }

    public static void main(String[] args) {
        List<Task> tasks = new LinkedList<Task>();

        while (true) {
            displayMenu();

            String option;
            option = checkOption();
    
            //go to the corresponding function
            switch (option) {
                case "1":
                    addTask((LinkedList<Task>) tasks);
                    continue;

                case "2":
                    deleteTask((LinkedList<Task>) tasks);
                    continue;
    
                case "3":
                    displayTask((LinkedList<Task>) tasks);
                    continue;
                    
                default:
                    break;
            }
            break;
        }

    }
}
