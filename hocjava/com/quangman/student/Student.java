package hocjava.com.quangman.student;
//J1.S.P0065

import java.util.Scanner;
import hocjava.com.quangman.calculator_bmi.Common;


public class Student {
    enum StudentType {
        A,
        B,
        C,
        D
    }

    private String name;
    private String _class;
    private double mathScore;
    private double chemistryScore;
    private double physicsScore;
    private double averageScore;
    private StudentType type;

    public StudentType getType() {
        return this.type;
    }

    public void setType(StudentType type) {
        this.type = type;
    }

    static Scanner scanner = new Scanner(System.in);

    public void input () {
        System.out.println("====== Management Student Program ======");
        System.out.print("Name: ");
        name = scanner.next();

        System.out.print("Class: ");
        _class = scanner.next();

        mathScore = Common.checkDoubleInput("Math: ", 0, 10);
        chemistryScore = Common.checkDoubleInput("Chemistry: ", 0, 10);
        physicsScore = Common.checkDoubleInput("Physics: ", 0, 10);

        averageScore = calculateAverageScore();
        type = calculateType();
    }

    private double calculateAverageScore() {
        return (mathScore + chemistryScore + physicsScore) / 3;
    }

    private StudentType calculateType() {
        if (averageScore > 7.5) {
            return StudentType.A;
        }

        if (averageScore >= 6) {
            return StudentType.B;
        }

        if (averageScore >= 4) {
            return StudentType.C;
        }

        return StudentType.D;
    }

    public void output() {
        System.out.println("Name: " + name);
        System.out.println("Class: " + _class);
        System.out.println("Average score: " + Common.decimalFormat.format(averageScore));
        System.out.println("Type: " + type);
    }

}
