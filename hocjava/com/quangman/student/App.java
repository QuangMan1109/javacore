package hocjava.com.quangman.student;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import hocjava.com.quangman.calculator_bmi.Common;

public class App {

    private static HashMap<String, Double> getPercentStudentType(List<Student> students) {

        int AQuantity = 0;
        int BQuantity = 0;
        int CQuantity = 0;
        int DQuantity = 0;
        for (Student student: students) {
            switch (student.getType()) {
                case A:
                    AQuantity++;
                    break;
            
                case B:
                    BQuantity++;
                    break;   
                    
                case C:
                    CQuantity++;
                    break;    

                case D:
                    DQuantity++;
                    break;

                default:
                    break;
            }
        }

        Map<String, Double> percentageByType = new HashMap<String, Double>();
        percentageByType.put("A", AQuantity*1.0 / students.size());
        percentageByType.put("B", BQuantity*1.0 / students.size());
        percentageByType.put("C", CQuantity*1.0 / students.size());
        percentageByType.put("D", DQuantity*1.0 / students.size());

        for (Entry entry : percentageByType.entrySet()) {
            System.out.println(entry.getKey() + ": " + Common.decimalFormat.format(entry.getValue()));
        }
        return (HashMap<String, Double>) percentageByType;
    }

    public static void main(String[] args) {
        List<Student> studentList = new ArrayList<Student>();
        boolean moreStudent = true;
        
        //input the list of students until the N key is entered
        while (moreStudent) {
            Student student = new Student();
            student.input();

            studentList.add(student);

            System.out.print("Do you want to enter more student information(Y/N)? ");
            String str = "";

            while (true) {
                str = Student.scanner.next();
                System.out.println("str = " + str);
                if (! (str.equals("Y") || str.equals("N"))) {
                    System.out.print("Only Y/N answer: ");
                    continue;
                }
                break;
            }

            if (str.equals("Y")) {
                moreStudent = true;
            }
            else {
                moreStudent = false;
            }

        }

        //print the list of students
        for (Student student: studentList) {
            student.output();
        }

        //print type A, B, C, D of the entire list by percentage
        getPercentStudentType(studentList);
    }
}
