// J1.S.P0051
// Author: Quang Man

package hocjava.com.quangman.calculator_bmi;


public class Calculator_BMI {

    private static int option;

    public static void displayMenu() {
        System.out.println("========= Calculator Program =========");
        System.out.println("1. Normal Calculator");
        System.out.println("2. BMI Calculator");
        System.out.println("3. Exit");

        chooseOption();
    }

    private static void chooseOption() {
        option = -1;
        while (option < 1 || option > 3){
            System.out.print("Please choice one option: ");
            option = Common.scanner.nextInt();
        }

        switch (option) {
            case 1:
                goToNormalCalculator();
                break;
        
            case 2:
                goToBMICalculator();
                break;

            default:
                break;
        }
    }

    private static void goToNormalCalculator() {
        
        double b, memory;
        Operator operator;

        memory = Common.checkDoubleInput("Enter number: ", Common.MIN_DOUBLE, Common.MAX_DOUBLE);
        operator = checkOperator();

        while (operator.c != '=') {
            b = Common.checkDoubleInput("Enter number: ", Common.MIN_DOUBLE, Common.MAX_DOUBLE);
            memory = calculate(memory, operator, b);
            System.out.println("Memory: " + Common.decimalFormat.format(memory));
            System.out.println("Please input (+, -, *, /, ^)");
            operator = checkOperator();
        }

        System.out.println("Result: " + memory);
        displayMenu();
    }

    private static void goToBMICalculator() {
        double weight, height;

        weight = Common.checkDoubleInput("Enter weight(kg): ", 0, 300);
        height = Common.checkDoubleInput("Enter height(cm): ", 0, 300);

        calculateBMI(weight, height);
        displayMenu();
    }


    public static Operator checkOperator() {

        while (true) {
            try {
                System.out.print("Enter operator: ");
                String myString = Common.scanner.next();

                if (myString.equals("+") || myString.equals("-") || myString.equals("*") || myString.equals("/") || myString.equals("^") || myString.equals("=")) {
                    return new Operator(myString.charAt(0));
                }
                else {
                    continue;
                }

            } catch (Exception e) {
                System.out.println("Incorrect data, try again: " + e);
                Common.scanner.next(); //clear the wrong input
                continue;
            }
        }
    } 

    public static double calculate (double a, Operator operator, double b) {
        switch (operator.c) {
            case '+':
                return (a + b);
            
            case '-':
                return (a - b);

            case '*':
                return (a * b);

            case '/':
                if (b == 0) {
                    System.out.println("Cannot divide by 0");
                    return a;
                }
                return (a / b);

            case '^':
                return Math.pow(a, b);

            default:
                return a;
        }
    }

    public static double calculateBMI(double weight, double height) {

        //print BMI index
        double BMI = (weight / (height / 100 * height / 100));
        System.out.println("BMI Number: " + Common.decimalFormat.format(BMI));

        //print BMI status
        if (BMI < Common.UNDER_STANDARD_MAX_BMI) {
            System.out.println("BMI Status: UNDER STANDARD");
        }
        else if (BMI < Common.STANDARD_MAX_BMI) {
            System.out.println("BMI Status: STANDARD");
        }
        else if (BMI < Common.OVERWEIGHT_MAX_BMI) {
            System.out.println("BMI Status: OVER WEIGHT");
        }
        else if (BMI < Common.FAT_MAX_BMI) {
            System.out.println("BMI Status: FAT");
        }
        else {
            System.out.println("BMI Status: VERY FAT");
        }

        return BMI;
    }

}

class Operator {
    public char c;
    
    Operator(char c) {
        this.c = c;
    }

    // public String getOperator() {
    //     return this.operator;
    // }

    // public void setOperator(String operator) {
    //     this.operator = operator;
    // }
}