package hocjava.com.quangman.calculator_bmi;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class Common {
    //scanner for input
    public static Scanner scanner = new Scanner(System.in);

    //beatify double number output
    public static DecimalFormat decimalFormat = new DecimalFormat("#.##");

    public static SimpleDateFormat myCalendarFormat = new SimpleDateFormat("dd-MM-yyyy");

    //min max for general use
    public static final double MAX_DOUBLE = 999999999L;
    public static final double MIN_DOUBLE = -999999999L;

    //for BMI project
    public static final double UNDER_STANDARD_MAX_BMI = 19;
    public static final double STANDARD_MAX_BMI = 25;
    public static final double OVERWEIGHT_MAX_BMI = 30;
    public static final double FAT_MAX_BMI = 40;

    //check double input with min & max constraints
    public static double checkDoubleInput(String consoleString, double minValue, double maxValue) {
        
        double myDouble;
        while (true) {
            try {
                System.out.print(consoleString);
                myDouble = scanner.nextDouble();

                //weight & height must be greater than 0
                if (!consoleString.contains("number")) {
                    if (myDouble < minValue || myDouble > maxValue) {
                        System.out.println("The number must be >= " + minValue + " and =< " + maxValue);
                        continue;
                    }
                }
                return myDouble;
            } catch (Exception e) {
                System.out.println("Input must be a double number, try again: " + e);
                scanner.next(); //clear the wrong input
                continue;
            }
        }
    }

    //check integer input with min & max constraints
    public static int checkIntegerInput(String consoleString, int minValue, int maxValue) {
        
        int myInt;
        while (true) {
            try {
                System.out.print(consoleString);
                myInt = scanner.nextInt();

                //weight & height must be greater than 0
                if (!consoleString.contains("number")) {
                    if (myInt < minValue || myInt > maxValue) {
                        System.out.println("The number must be >= " + minValue + " and =< " + maxValue);
                        continue;
                    }
                }
                return myInt;
            } catch (Exception e) {
                System.out.println("Input must be a integer number, try again: " + e);
                scanner.next(); //clear the wrong input
                continue;
            }
        }
    }
}
