package hocjava.com.quangman.sort_search_array;
public class SelectionSort {
    public static void run(int arr[]) {
        int n = arr.length;

        for (int i = 0; i < n - 1; i++) {
            int min = arr[i];
            int minIndex = i;
            for (int j = i; j < n; j++) {
                if (min > arr[j]) {
                    min = arr[j];
                    minIndex = j;
                }
            }

            Common.swapArray(arr, i, minIndex);

        }

    }
}
