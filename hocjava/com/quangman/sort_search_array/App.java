package hocjava.com.quangman.sort_search_array;
import java.util.Random;
import java.util.Scanner;

public class App {
    enum SortType {
        BUBBLE_SORT,
        SELECTION_SORT,
        QUICK_SORT
    }

    private static final Scanner scanner = new Scanner(System.in);
    private static int n;
    private static int arr[];
    private static int searchValue;

    public static void main(String[] args) {

        sortMain(SortType.BUBBLE_SORT);

    }

    private static void enterN() {
        n = -1;
        while (n < 2) {
            System.out.print("Enter n > 1: ");
            n = scanner.nextInt();
        }
    }

    private static void enterArr(boolean isRandomArray) {
        arr = new int[n];

        if (isRandomArray) {
            for (int i = 0; i < n; i++) {
                arr[i] = new Random().nextInt(Common.MAX) - Common.MAX/2;
            }
        }
        else {
            for (int i = 0; i < n; i++) {
                arr[i] = scanner.nextInt();
            }
        }
    }

    private static void enterSearchValue() {
        searchValue = scanner.nextInt();
    }

    private static void sortMain(SortType sortType) {
        enterN();
        enterArr(true);
        scanner.close();

        System.out.println("Before sorting:");
        Common.printArray(arr);

        switch (sortType) {
            case BUBBLE_SORT:
                BubbleSort.run(arr);
                break;

            case SELECTION_SORT:
                SelectionSort.run(arr);
                break;
        
            case QUICK_SORT:
                QuickSort.run(arr);
                break;

            default:
                break;
        }

        System.out.println("After sorting:");
        Common.printArray(arr);
    }

    private static void binarySearch_main() {
        enterN();
        enterArr(true);


        QuickSort.run(arr);

        System.out.println("After sorting:");
        Common.printArray(arr);

        enterSearchValue();
        scanner.close();

        System.out.println("search index = " + BinarySearch.run(arr, searchValue));
    }
}
