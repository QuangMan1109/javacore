package hocjava.com.quangman.sort_search_array;
public class BubbleSort {

    public static void run(int arr[]) {
        int n = arr.length;
        for (int i = 0; i < n - 1 ; i++) {
            for (int j = i + 1; j < n; j++) {
                if (arr[i] > arr[j]) {
                    Common.swapArray(arr, i, j);
                }
            }
        }
    }

}
