package hocjava.com.quangman.sort_search_array;
public class Common {
    public static final int MAX = 2000;  //For easy observation in terminal
    

    public static void printArray(int arr[]) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "\t");
        }
        System.out.println("");
    }


    public static void swapArray(int arr[], int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp; 
    }
}
