package hocjava.com.quangman.sort_search_array;
public class QuickSort {
    public static void run(int arr[]) {
        QuickSort.sortPartitionOfArray(arr, 0, arr.length - 1);

    }

    private static void sortPartitionOfArray(int arr[], int left, int right) {
        if (left < right) {
            int partitionIndex = QuickSort.movePivotToRightPlace(arr, left, right);

            QuickSort.sortPartitionOfArray(arr, left, partitionIndex - 1);
            QuickSort.sortPartitionOfArray(arr, partitionIndex + 1, right);
        }
    }

    private static int movePivotToRightPlace(int arr[], int left, int right) {
        int pivotIndex = right; //Choose pivot element is the last element of the array
        int pivot = arr[pivotIndex];

        int i = left;

        for (int j = left; j < right; j++) {
            if (arr[j] < pivot) {                
                Common.swapArray(arr, i, j);
                i++;
            }
        }

        Common.swapArray(arr, i, pivotIndex);
        
        return i;

    }
}
