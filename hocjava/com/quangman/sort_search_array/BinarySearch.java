package hocjava.com.quangman.sort_search_array;
public class BinarySearch {
    public static int run(int arr[], int searchValue) {

        QuickSort.run(arr);
        
        return searchInPartition(arr, 0, arr.length - 1, searchValue);
    }

    private static int searchInPartition(int arr[], int left, int right, int searchValue) {
        if (left > right) {
            return -1;
        }
        
        int searchIndex = (left + right) / 2;

        if (arr[searchIndex] == searchValue) {
            return searchIndex;
        }
        else if (arr[searchIndex] > searchValue) {
            return searchInPartition(arr, left, right - 1, searchValue);
        }
        else {
            return searchInPartition(arr, left + 1, right, searchValue);
        }
    }
}
